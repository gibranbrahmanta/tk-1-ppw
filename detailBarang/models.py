from django.db import models
import datetime

# Create your models here.

class ProductDetail(models.Model):
    date = models.DateField(True, default=datetime.date.today)
    comment = models.CharField(max_length = 100, blank=False)
