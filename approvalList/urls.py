from django.urls import path
from .views import approval_list


appname = 'approvalList'

urlpatterns = [
   path('approvalList', approval_list, name = 'approvalList'),
]