from django.shortcuts import render,redirect 
from .models import Message as pesan
from .forms import MessageForm, UserForm


def approval_list(request):
    if request.method == "POST":
        form = MessageForm(request.POST)
        if form.is_valid():
            msg = pesan()
            msg.message = form.cleaned_data['message_for_user']
            msg.save()
        return redirect('/approvalList')
    else:
        form = MessageForm()
        user = UserForm()
        response = {'form' : form,'user':user}
        return render(request, 'approvalList.html', response)
