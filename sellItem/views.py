from django.shortcuts import render,redirect
from .form import addItem
from .models import Category

# Create your views here.
def saveItem(request, *args, **kwargs ):
	if request.method == 'POST':
		form = addItem(request.POST)
		if form.is_valid():
			categorys = Category()
			categorys.name = form.cleaned_data['name']
			categorys.prize = form.cleaned_data['prize']
			categorys.category = form.cleaned_data['category']
			categorys.describtion= form.cleaned_data['describtion']
			categorys.image = form.cleaned_data['image']
			categorys.save()
		return redirect('/saveItem')
	else:
		form = addItem()
	categorys = Category.objects.all()
	context ={
		'form' : form,
		'category' : categorys
	}
	return render(request, 'sellitem.html', context )