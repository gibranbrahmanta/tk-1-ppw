from django.db import models

# Create your models here.
class Category(models.Model):
    name = models.TextField()
    prize = models.TextField()

    DRESS = 'Dress'
    T_SHIRT = 'T-Shirt'
    SKIRT = 'Skirt'
    JUMPSUIT = 'Jumpsuit'
    PANTS_LEGGINGS = 'Pants & Leggings'
    JEANS = 'Jeans'
    SWIMWEAR = 'Swimwear'
    KNITWEAR_CARDIGANS = 'Knitwear & Cardigans'
    HOODIES_SWEATSHIRTS = 'Hoodies & Sweatshirts'
    BLAZER = 'Blazer'
    JACKET_COATS = 'Jacket & Coats'
    SLEEPWEAR = 'Sleepwear'
    ACCESSORIES = 'Accessories'
    ALL = 'All'

    CATEGORY_CHOICES = [
    (DRESS, 'Dress'), (T_SHIRT, 'T-Shirt'),
    (SKIRT, 'Skirt'), (JUMPSUIT, 'Jumpsuit'),
    (PANTS_LEGGINGS, 'Pants & Leggings'),
    (JEANS, 'Jeans'), (SWIMWEAR, 'Swimwear'),
    (KNITWEAR_CARDIGANS, 'Knitwear & Cardigans'),
    (HOODIES_SWEATSHIRTS, 'Hoodies & Sweatshirts'),
    (BLAZER, 'Blazer'), (JACKET_COATS, 'Jacket & Coats'),
    (SLEEPWEAR, 'Sleepwear'), (ACCESSORIES, 'Accessories'),
    (ALL, 'All'),
    ]

    category = models.CharField(max_length = 100,
        choices=CATEGORY_CHOICES, default = ALL,
        )

    describtion = models.TextField()
    image = models.ImageField()