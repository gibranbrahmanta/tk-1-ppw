from django import forms
from .models import Category

class addItem(forms.Form):
    attr = {
    'class' : 'form-control'
    }

    name = forms.CharField(label='Item Name', required=True, widget=forms.TextInput(attrs=attr))
    prize = forms.CharField(label='Rent Prize (per month)', required=True, widget=forms.TextInput(attrs=attr))

    DRESS = 'Dress'
    T_SHIRT = 'T_Shirt'
    SKIRT = 'Skirt'
    JUMPSUIT = 'Jumpsuit'
    PANTS_LEGGINGS = 'Pants & Legging'
    JEANS = 'Jeans'
    SWIMWEAR ='Swimwear'
    KNITWEAR_CARDIGANS = 'Knitwear & Cardigans'
    HOODIES_SWEATSHIRTS = 'Hoodies & Sweatshirts'
    BLAZER = 'Blazer'
    JACKET_COATS = 'Jacket & Coats'
    SLEEPWEAR = 'Sleepwear'
    ACCESSORIES = 'Accessories'
    ALL = 'All'

    CATEGORY_CHOICES = [
    (DRESS, 'Dress'), (T_SHIRT, 'T-Shirt'),
    (SKIRT, 'Skirt'), (JUMPSUIT, 'Jumpsuit'),
    (PANTS_LEGGINGS, 'Pants & Leggings'),
    (JEANS, 'Jeans'), (SWIMWEAR, 'Swimwear'),
    (KNITWEAR_CARDIGANS, 'Knitwear & Cardigans'),
    (HOODIES_SWEATSHIRTS, 'Hoodies & Sweatshirts'),
    (BLAZER, 'Blazer'), (JACKET_COATS, 'Jacket & Coats'),
    (SLEEPWEAR, 'Sleepwear'), (ACCESSORIES, 'Accessories'),
    (ALL, 'All'),
    ]

    category = forms.ChoiceField(choices=CATEGORY_CHOICES)

    describtion = forms.CharField(label='Item Describtion', required=True, widget=forms.TextInput(attrs=attr))
    image = forms.ImageField(label = 'Upload Photo')


