"""TK_1_PPW URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

# static files
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',include('homepage.urls')),
    path('',include('detailBarang.urls')),
    path('',include('approvalList.urls')),
    path('',include('startsell.urls')),
    path('',include('sellItem.urls')),
    path('products/', include('products.urls')),
    path('product-detail/', include('detailBarang.urls'))
]

urlpatterns += staticfiles_urlpatterns()