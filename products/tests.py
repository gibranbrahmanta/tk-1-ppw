from django.test import TestCase

# Create your tests here.
class ProductsTest(TestCase):
    def test_apakah_ada_url_slash_products(self):
        c = Client()
        response = c.get('/products')
        self.assertEqual(response.status_code, 200)