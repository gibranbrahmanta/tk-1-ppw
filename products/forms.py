from django import forms
# import models nya via

class CategoryForm(forms.Form):
    DRESS = 'Dress'
    TSHIRT = 'T-Shirt'
    SKIRT = 'Skirt'
    JUMPSUIT = 'Jumpsuit'
    PANTS_LEGGINGS = 'Pants & Leggings'
    JEANS = 'Jeans'
    SWIMWEAR = 'Swimwear'
    KNITWEAR_CARDIGANS = 'Knitwear & Cardigans'
    HOODIES_SWEATSHIRTS = 'Hoodies & Sweatshirts'
    BLAZER = 'Blazer'
    JACKET_COATS = 'Jacket & Coats'
    SLEEPWEAR = 'Sleepwear'
    ACCESSORIES = 'Accessories'
    ALL = 'All'

    CATEGORY_CHOICES = [
        (DRESS, 'Dress'),
        (TSHIRT, 'T-Shirt'),
        (SKIRT, 'Skirt'),
        (JUMPSUIT, 'Jumpsuit'),
        (PANTS_LEGGINGS, 'Pants & Leggings'),
        (JEANS, 'Jeans'),
        (SWIMWEAR, 'Swimwear'),
        (KNITWEAR_CARDIGANS, 'Knitwear & Cardigans'),
        (HOODIES_SWEATSHIRTS, 'Hoodies & Sweatshirts'),
        (BLAZER, 'Blazer'),
        (JACKET_COATS, 'Jacket & Coats'),
        (SLEEPWEAR, 'Sleepwear'),
        (ACCESSORIES, 'Accessories'),
        (ALL, 'All'),
    ]

    Filter_by_category = forms.ChoiceField(choices=CATEGORY_CHOICES)