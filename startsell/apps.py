from django.apps import AppConfig


class StartsellConfig(AppConfig):
    name = 'startsell'
