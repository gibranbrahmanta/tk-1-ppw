from django.urls import path
from . import views

app_name = 'startsell'

urlpatterns= [
	path('startsell', views.startsell, name = 'startsell')
	]